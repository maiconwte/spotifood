const toJSON = (response: Response): Promise<Response> => response.json();

export default toJSON;
