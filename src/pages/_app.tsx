import React from 'react';
import { AppProps } from 'next/app';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from '@/styles/theme';
import GlobalStyles from '@/styles/Global';

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => (
  <ThemeProvider theme={theme}>
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Component {...pageProps} />
    </MuiPickersUtilsProvider>
    <GlobalStyles />
  </ThemeProvider>
);

export default MyApp;
