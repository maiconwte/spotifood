import React from 'react';
import { GetServerSideProps } from 'next';
import Container from '@material-ui/core/Container';
import { Grid } from '@material-ui/core';

import SEO from '@/components/SEO';
import PlaylistsFilters from '@/components/PlaylistsFilters';
import FeaturedPlaylists from '@/components/FeaturedPlaylists';
import LoginWithSpotify from '@/components/LoginWithSpotify';

import { Query } from '@/hooks/useFeaturedPlaylists';

import { Main, LoginBox } from '@/styles/pages';

export const getServerSideProps: GetServerSideProps = async ({ query }) => ({
  props: {
    query,
    token: query.access_token || null,
  },
});

interface Props {
  query?: Query;
  token: string;
}

const Home: React.FC<Props> = ({ query, token }) => {
  const isAuthenticated = token;

  return (
    <Container>
      <SEO title="Spotifood" />

      <Main>
        <Grid container spacing={4}>
          <Grid item xs={12} md={6}>
            <PlaylistsFilters />
          </Grid>
          <Grid item xs={12} md={6}>
            {(() => {
              if (isAuthenticated) {
                return (
                  <FeaturedPlaylists
                    query={query}
                  />

                );
              }

              return (
                <LoginBox>
                  <LoginWithSpotify />
                </LoginBox>
              );
            })()}
          </Grid>
        </Grid>
      </Main>
    </Container>
  );
};

export default Home;
