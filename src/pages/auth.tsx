import React, { useEffect } from 'react';
import Container from '@material-ui/core/Container';

import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

const Auth: React.FC = () => {
  const router = useRouter();

  useEffect(() => {
    const { location } = window;
    const token = location.hash.split('#access_token=')[1];

    if (token) {
      router.push({
        pathname: '/',
        query: {
          ...router.query,
          access_token: encodeURIComponent(token),
        },
      });
    }
  }, [router]);

  return (
    <Container>
      <Typography
        variant="h1"
        style={{ marginTop: '30px' }}
      >
        Authenticating ... You will be redirected soon
        </Typography>
    </Container>
  );
};

export default Auth;
