import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
   html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed,
    figure, figcaption, footer, header, hgroup,
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }

    /* HTML5 display-role reset for older browsers */
    article, aside, details, figcaption, figure,
    footer, header, hgroup, menu, nav, section {
        display: block;
    }

    body {
      line-height: 1;
      min-height: 100vh;
      font-family: --apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;

      color: var(--text-primary);
    }

    ol, ul {
        list-style: none;
    }

    blockquote, q {
        quotes: none;
    }

    blockquote:before,
    blockquote:after,
    q:before,
    q:after {
        content: '';
        content: none;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    * {
        box-sizing: border-box;
    }

    :root {
      --primary: #1DB954;
      --background-dark: #121212;
      --container-max-width: 1366px;
      --text-secondary: #717171;
      --text-primary: #000;

      --color-gray-medium: #2A2A2A;
      --color-gray-light: #d9dadc;
      --color-error: #EA1D2C;

      --grid-sm: 768px;
      --grid-md: 992px;
      --grid-lg: 1200px;

      .MuiTypography-h1 {
        font-size: 30px;
        font-weight: 900;
        letter-spacing: -.02em;
        line-height: 1.2;
        margin-bottom: 30px;

        @media (min-width: 992px) {
          font-size: 48px;
        }
      }

      .MuiTypography-h2 {
        font-size: 28px;
        font-weight: 700;
        line-height: 28px;
        margin-bottom: 30px;
        color: var(--text-primary);
      }

      .MuiTypography-h4, .MuiTypography-h5 {
        word-wrap: break-word;
      }

      .MuiTypography-body1 {
        margin-bottom: 15px;
        line-height: 1.5;
        color: var(--color-gray-medium);
      }

      .MuiTypography-body2 {
        font-weight: 700;
        color: var(--color-gray-medium);
      }
    }
`;
