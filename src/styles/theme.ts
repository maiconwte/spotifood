import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#1DB954',
    },
    error: {
      main: '#ea1d2c',
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
