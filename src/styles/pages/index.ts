import styled from 'styled-components';

export const Main = styled.main`
  padding-top: 30px;
  position: relative;

  section {
    &#playlist-filters {
      @media (min-width: 992px) {
        position: sticky;
        top: 60px;
      }
    }
  }

  @media (min-width: 992px) {
    padding-top: 60px;
  }
`;

export const LoginBox = styled.div`
  display: flex;
  height: 100%;
  justify-content: center;
  align-items: center;
`;
