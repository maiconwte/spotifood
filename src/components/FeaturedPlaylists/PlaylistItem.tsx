import React from 'react';

import { Playlist } from '@/hooks/useFeaturedPlaylists';

import { ListItem } from './styles';

interface Props {
  playlist: Playlist
}

const PlaylistItem: React.FC<Props> = ({ playlist }) => (
  <ListItem>
    <a
      href={playlist.external_urls.spotify}
      title={playlist.name}
      target="blank"
    >
      <div className="img-rounded">
        <img src={playlist.images[0].url} alt={playlist.name} />
      </div>

      <div className="playlist-detail">
        <p data-testid="playlist-name">
          {playlist.name}
        </p>
        <small data-testid="playlist-description">
          {playlist.description}
        </small>
      </div>
    </a>
  </ListItem>
);

export default PlaylistItem;
