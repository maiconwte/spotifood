import React, { useState, memo } from 'react';
import {
  Box, Typography, FormControl, TextField, List,
} from '@material-ui/core';

import { SWRErrorBoundarie } from '@/components/Error';
import useFeaturedPlaylists, { Query } from '@/hooks/useFeaturedPlaylists';

import Empty from '@/components/Empty';

import Loading from './Loading';
import PlaylistItem from './PlaylistItem';

interface Props {
  query?: Query
}

const FeaturedPlaylists: React.FC<Props> = ({ query }) => {
  const [inputValue, setInputValue] = useState('');
  const {
    title, playlists, loading, error,
  } = useFeaturedPlaylists({
    query,
  });
  const isEmpty = !playlists || !playlists.length;

  if (error) {
    return (
      <SWRErrorBoundarie
        description="Something went wrong on fetch featured playlists items:"
        error={error}
      />
    );
  }

  if (loading) {
    return <Loading />;
  }

  if (isEmpty) {
    return (
      <Empty
        title="Sorry. No playlist was found."
        subtitle="Try searching using other filters."
      />
    );
  }

  return (
    <Box id="feature-playlists">
      <Typography variant="h2">
        {title}
      </Typography>

      <FormControl fullWidth variant="outlined" style={{ marginBottom: 35 }}>
        <TextField
          id="search-playlist"
          variant="outlined"
          label="Search Playlist"
          type="search"
          size="small"
          placeholder="Search playlists by name"
          value={inputValue}
          onChange={({ target }) => setInputValue(target.value)}
        />
      </FormControl>

      <List>
        {(() => {
          if (!inputValue) {
            return playlists
              .map((playlist) => (
                <PlaylistItem key={playlist.id} playlist={playlist} />
              ));
          }

          const filteredPlaylists = playlists
            .filter(
              (playlist) => playlist
                .name
                .toLocaleLowerCase()
                .includes(inputValue.toLocaleLowerCase()),
            );

          if (!filteredPlaylists.length) {
            return (
              <Empty
                title={`No playlist found with name: "${inputValue}"`}
                subtitle="Try searching using other term"
              />
            );
          }

          return filteredPlaylists.map((playlist) => (
            <PlaylistItem key={playlist.id} playlist={playlist} />
          ));
        })()}
      </List>
    </Box>
  );
};

export default memo(FeaturedPlaylists);
