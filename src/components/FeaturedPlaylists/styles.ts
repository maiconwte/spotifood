import styled from 'styled-components';

export const ListItem = styled.li`
  cursor: pointer;

  a, .shimmer {
    display: flex;
    align-items: center;
    min-height: 76px;
    text-decoration: none;
  }

  .img-rounded {
    width: 60px;
    height: 60px;
    margin-right: 10px;
    border-radius: 15%;
    overflow: hidden;
  }

  .playlist-detail {
    flex: 1;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  p {
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: normal;
    color: var(--text-primary);
  }

  small {
    font-size: 14px;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: normal;
    text-transform: none;
    color: var(--text-secondary);
  }

  p, small {
    word-break: break-all;
    transition: color .25s ease;
  }

  &:hover {
    p, small {
      color: var(--primary);
    }
  }
`;
