import React from 'react';
import { List, Typography } from '@material-ui/core';

import { Skeleton } from '@material-ui/lab';

import { ListItem } from './styles';

const Loading: React.FC = () => (
  <div data-testid="loading-feature-playlists">
    <Typography variant="h2">
      <Skeleton variant="rect" height={28} />
    </Typography>

    <Skeleton
      variant="rect"
      height={40}
      style={{ marginBottom: 35 }}
    />

    <List>
      {[1, 2, 3, 4].map((i) => (
        <ListItem key={i}>
          <div className="shimmer">
            <div className="img-rounded">
              <Skeleton variant="rect" width={60} height={60} />
            </div>

            <div className="playlist-detail">
              <p data-testid="playlist-name">
                <Skeleton />
              </p>
              <small data-testid="playlist-description">
                <Skeleton />
              </small>
            </div>
          </div>
        </ListItem>
      ))}
    </List>
  </div>
);

export default Loading;
