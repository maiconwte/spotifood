import React from 'react';
import { Box, Typography } from '@material-ui/core';

interface Props {
  title?: string;
  subtitle?: string;
}

const Empty: React.FC<Props> = ({ title = 'Nothing to show.', subtitle }) => (
  <Box textAlign="center" css={{ paddingTop: 30, paddingBottom: 30 }}>
    <Typography
      variant="h5"
      color="error"
      gutterBottom
    >
      {title}
    </Typography>
    {subtitle
      && <Typography variant="body1">{subtitle}</Typography>
    }
  </Box>
);

export default Empty;
