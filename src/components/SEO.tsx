import React from 'react';
import Helmet from 'react-helmet';

interface Meta {
  name: string;
  content: string;
}

interface Props {
  description?: string;
  lang?: string;
  meta?: Meta[];
  title?: string;
  image?: string;
}

const SEO: React.FC<Props> = ({
  description, lang = 'pt-br', meta = [], title, image,
}) => {
  const site = {
    siteMetadata: {
      title: 'ifood-frontend-test',
      author: 'Maicon Silva',
      description: 'Get preferred playlists from iFood`s customers',
      siteUrl: 'https://www.maiconsilva.com',
    },
  };

  const metaDescription = description || site.siteMetadata.description;
  const url = site.siteMetadata.siteUrl;
  const ogImage = `${url}${image || '/static/images/github-logo.svg'}`;
  const language = lang;

  return (
    <Helmet
      htmlAttributes={{
        lang: language,
      }}
      title={title || site.siteMetadata.title}
      titleTemplate={`${title ? '%s | ' : ''}${site.siteMetadata.title}`}
      meta={[
        {
          name: 'theme-color',
          content: '#1DB954',
        },
        {
          name: 'description',
          content: metaDescription,
        },
        {
          property: 'og:title',
          content: title,
        },
        {
          property: 'og:description',
          content: metaDescription,
        },
        {
          property: 'og:image',
          content: ogImage,
        },
        {
          property: 'og:type',
          content: 'website',
        },
        {
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          name: 'twitter:image:src',
          content: ogImage,
        },
        {
          name: 'twitter:creator',
          content: site.siteMetadata.author,
        },
        {
          name: 'twitter:title',
          content: title,
        },
        {
          name: 'twitter:description',
          content: metaDescription,
        },
      ].concat(meta)}
    />
  );
};

export default SEO;
