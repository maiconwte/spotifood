import React from 'react';
import { Button } from '@material-ui/core';

const clientId = 'd55e2c7813a54b61aad651ce121fb382';
const redirectURI = process.env.NODE_ENV === 'development'
  ? 'http://localhost:3000/auth'
  : 'https://spotifood.maiconrs95.vercel.app/auth';

const authParams = `client_id=${clientId}&response_type=token&redirect_uri=${redirectURI}`;

const LoginWithSpotify: React.FC = () => (
  <Button
    style={{
      minWidth: '250px',
      padding: '12px 14px 12px',
      color: '#fff',
      borderRadius: '500px',
      fontWeight: 700,
      textTransform: 'uppercase',
      fontSize: '14px',
    }}
    variant="contained"
    color="primary"
    href={`https://accounts.spotify.com/authorize?${authParams}`}
    disableElevation
  >
    Login With Spotify
  </Button>
);

export default LoginWithSpotify;
