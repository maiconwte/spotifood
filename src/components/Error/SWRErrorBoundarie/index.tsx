import React from 'react';
import { Typography, Box } from '@material-ui/core';

import LoginWithSpotify from '@/components/LoginWithSpotify';

interface Props {
  title?: string;
  description?: string;
  error?: Error;

}
const SWRErrorBoundarie: React.FC<Props> = ({
  title = 'Ooops!',
  description = 'Something went wrong. The following error occurred on fetch data items:',
  error,
  children,
}) => {
  if (error) {
    return (
      <Box
        data-testid="error-wrapper"
        textAlign="center"
        css={{ margin: '0 auto', maxWidth: '400px' }}
      >
        <Typography variant="h4" color="error" gutterBottom>
          {title}
        </Typography>
        <Typography variant="body1">
          {description}
        </Typography>
        {(() => {
          switch (error.message.toLocaleLowerCase()) {
            case 'the access token expired':
            case 'only valid bearer authentication supported':
            case 'invalid access token':
              return (
                <>
                  <Typography variant="body2" style={{ marginBottom: '20px' }}>
                    {error.message}
                  </Typography>
                  <LoginWithSpotify />
                </>
              );

            default:
              return (
                <Typography variant="body2">
                  {error.message}
                </Typography>
              );
          }
        })()}
      </Box>
    );
  }

  return <>{children}</>;
};

export default SWRErrorBoundarie;
