import React, {
  useEffect, useState, memo,
} from 'react';
import { Skeleton } from '@material-ui/lab';
import { Typography } from '@material-ui/core';

import api from '@/services/api';
import FilterField, { Field } from '@/components/FilterField';

const PlaylistsFilters: React.FC = () => {
  const [fields, setFields] = useState<Field[]>([] as Field[]);
  const [loading, setLoading] = useState(true);
  const [, setError] = useState(null);

  useEffect(() => {
    (async () => {
      setLoading(true);
      try {
        const { data } = await api.get('/v2/5a25fade2e0000213aa90776', {
          baseURL: 'https://www.mocky.io',
        });

        setFields(data.filters);
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return (
    <section id="playlist-filters">
      <Typography variant="h1">Get preferred playlists from iFood&apos;s customers</Typography>
      {(() => {
        const isEmpty = !fields || !fields.length;

        if (loading) {
          return (
            <div data-testid="loading-playlists-filters">
              {[1, 2, 3, 4, 5].map((i) => (
                <div key={i} style={{ marginBottom: '25px' }}>
                  <Skeleton variant="rect" height={56} />
                </div>
              ))}
            </div>
          );
        }

        if (isEmpty) {
          return (
            <Typography variant="body1" gutterBottom>
              No filters available
            </Typography>
          );
        }

        return fields?.map((filter) => (
          <FilterField
            key={filter.id}
            filter={filter}
          />
        ));
      })()}
    </section>
  );
};

export default memo(PlaylistsFilters);
