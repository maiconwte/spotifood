import React from 'react';
import { useRouter } from 'next/router';
import { DateTimePicker } from '@material-ui/pickers';
import formatISO from 'date-fns/formatISO';
import {
  FormControl, InputLabel, TextField, Select,
} from '@material-ui/core';

interface FieldValues {
  value: string;
  name: string;
}

interface Validation {
  min: number;
  max: number;
  entityType?: string;
  primitiveType?: string;
}

export interface Field {
  id?: 'locale' | 'country' | 'timestamp' | 'limit' | 'offset' | undefined;
  name: string;
  values?: FieldValues[];
  validation?: Validation;
}

interface Props {
  filter?: Field;
}

const FilterField: React.FC<Props> = ({ filter }) => {
  const router = useRouter();
  const { query } = router;

  const handleQuery = ({ key, value }) => {
    router.push({
      query: {
        ...query,
        [key]: value,
      },
    });
  };

  const inputChange = ({ target }) => handleQuery({
    key: target.name,
    value: target.value,
  });

  const dateChange = (e) => handleQuery({
    key: filter.id,
    value: formatISO(e),
  });

  return (
    <FormControl fullWidth variant="outlined" style={{ marginBottom: 25 }}>
      {(() => {
        switch (filter.id) {
          case 'locale':
          case 'country':
            return (
              <>
                <InputLabel id={filter.id} htmlFor={filter.id}>
                  {filter.name}
                </InputLabel>
                <Select
                  native
                  data-testid={filter.id}
                  inputProps={{
                    name: filter.id,
                    id: filter.id,
                    defaultValue: query[filter.id],
                  }}
                  label={filter.name}
                  onChange={inputChange}
                >
                  <option aria-label="None" value="" />
                  {filter.values.map((value) => (
                    <option value={value.value} key={value.name}>
                      {value.name}
                    </option>
                  ))}
                </Select>
              </>
            );

          case 'timestamp':
            return (
              <DateTimePicker
                data-testid="timestamp"
                inputVariant="outlined"
                inputProps={{
                  id: filter.id,
                  name: filter.id,
                }}
                label={filter.name}
                format="dd/MM/yyyy HH:mm:ss"
                value={
                  query[filter.id]
                    ? query[filter.id] : null
                }
                onChange={dateChange}
              />
            );

          case 'limit':
            return (
              <TextField
                id={filter.id}
                name={filter.id}
                label={filter.name}
                data-testid="limit"
                variant="outlined"
                type="number"
                inputProps={{
                  min: filter.validation.min,
                  max: filter.validation.max,
                }}
                defaultValue={query[filter.id] || 20}
                onChange={inputChange}
              />
            );

          case 'offset':
            return (
              <TextField
                id={filter.id}
                name={filter.id}
                label={filter.name}
                data-testid="offset"
                variant="outlined"
                type="number"
                inputProps={{
                  min: 0,
                }}
                defaultValue={query[filter.id] || 0}
                onChange={inputChange}
              />
            );

          default:
            return <TextField data-testid="default" />;
        }
      })()}
    </FormControl>
  );
};

export default FilterField;
