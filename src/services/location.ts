export const getHashToken = (key: string): string => {
  const params = new URLSearchParams(window.location.search.substring(1));
  const value = params.get(key);

  if (value) {
    return decodeURIComponent(value);
  }

  return null;
};
