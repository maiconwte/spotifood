import axios from 'axios';

export const headers = {
  Authorization: `Bearer ${process.env.SPOTIFY_BEARER_TOKEN}`,
};

const api = axios.create({
  baseURL: 'https://api.spotify.com/v1/browse',
  headers,
});

export default api;
