import useSWR from 'swr';
import formatISO from 'date-fns/formatISO';

import api from '@/services/api';
import { getHashToken } from '@/services/location';

export interface Query {
  country: string;
  locale: string;
  timestamp: string;
  limit: number;
  offset: number;
}

interface ExternalUrls {
  spotify?: string;
}

export interface Playlist {
  id: string;
  name: string;
  images: Array<{ url: string }>;
  external_urls: ExternalUrls;
  description: string;
}

interface Props {
  query?: Query;
}

interface UseFeaturedPlaylists {
  loading: boolean;
  title: string;
  playlists: Playlist[];
  error: Error;
}

const swrOptions = {
  refreshInterval: 30000,
  shouldRetryOnError: false,
  revalidateOnFocus: false,
};

const acceptedParams = ['locale', 'country', 'limit', 'timestamp', 'offset'];

const useFeaturedPlaylists = ({ query }: Props): UseFeaturedPlaylists => {
  const qs = Object
    .keys(query)
    .filter((key) => !!query[key])
    .filter((key) => acceptedParams.includes(key))
    .map((key) => {
      const value = key === 'timestamp'
        ? formatISO(new Date(query[key])) : query[key];

      return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
    })
    .join('&');

  const { data, error } = useSWR(
    `featured-playlists?${qs}`,
    async (url) => {
      const spotifyToken = getHashToken('access_token');

      if (spotifyToken) {
        api.defaults.headers.Authorization = `Bearer ${spotifyToken}`;
      }

      try {
        const response = await api.get(url);
        const title = response.data.message;
        const playlists = response.data.playlists.items;

        return { title, playlists };
      } catch ({ response }) {
        throw new Error(response.data.error.message);
      }
    },
    swrOptions,
  );

  return {
    title: data && data.title,
    playlists: data && data.playlists,
    loading: !data && !error,
    error,
  };
};

export default useFeaturedPlaylists;
