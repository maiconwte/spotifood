import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import FilterField from '@/components/FilterField';

jest.mock('next/router', () => ({
  useRouter: () => ({
    push: jest.fn(),
    query: {},
  }),
}));

const wrapper = ({ children }) => (
  <MuiPickersUtilsProvider utils={DateFnsUtils}>
    {children}
  </MuiPickersUtilsProvider>
);

describe('<FilterField />', () => {
  it('should render "default" input', () => {
    const { getByTestId } = render(
      <FilterField filter={{ name: 'default' }} />,
    );

    expect(getByTestId('default')).toBeInTheDocument();
  });

  it('should render "locale" input', () => {
    const { getByTestId } = render(
      <FilterField
        filter={{
          id: 'locale',
          name: 'input name',
          values: [
            { name: 'Locale 1', value: 'Value 1' },
            { name: 'Locale 2', value: 'Value 1' },
            { name: 'Locale 3', value: 'Value 1' },
          ],

        }}

      />,
      { wrapper },
    );

    expect(getByTestId('locale')).toBeInTheDocument();
  });

  it('should render "country" input', () => {
    const { getByTestId } = render(
      <FilterField
        filter={{
          id: 'country',
          name: 'input name',
          values: [
            { name: 'Country 1', value: 'Value 1' },
            { name: 'Country 2', value: 'Value 1' },
            { name: 'Country 3', value: 'Value 1' },
          ],
        }}

      />,
      { wrapper },
    );

    expect(getByTestId('country')).toBeInTheDocument();
  });

  it('should render "timestamp" input', () => {
    const { getByTestId } = render(
      <FilterField
        filter={{
          id: 'timestamp',
          name: 'input name',
        }}

      />,
      { wrapper },
    );

    expect(getByTestId('timestamp')).toBeInTheDocument();
  });

  it('should render "limit" input', () => {
    const { getByTestId } = render(
      <FilterField
        filter={{
          id: 'limit',
          name: 'input name',
          validation: {
            min: 0,
            max: 20,
          },
        }}

      />,
      { wrapper },
    );

    expect(getByTestId('limit')).toBeInTheDocument();
  });

  it('should render "offset" input', () => {
    const { getByTestId } = render(
      <FilterField
        filter={{
          id: 'offset',
          name: 'input name',
          validation: {
            min: 0,
            max: 20,
          },
        }}

      />,
      { wrapper },
    );

    expect(getByTestId('offset')).toBeInTheDocument();
  });
});
