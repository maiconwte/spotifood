import React from 'react';
import { render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import PlaylistsFilters from '@/components/PlaylistsFilters';

const wrapper = ({ children }) => (
  <MuiPickersUtilsProvider utils={DateFnsUtils}>
    {children}
  </MuiPickersUtilsProvider>
);

jest.mock('@/services/api', () => ({
  get: jest.fn()
    .mockImplementationOnce(() => ({ data: { filters: [] } }))
    .mockResolvedValueOnce({
      data: {
        filters:
          [
            {
              id: 'locale',
              name: 'Locale',
              values: [
                {
                  value: 'en_AU',
                  name: 'en_AU',
                },
              ],
            },
            {
              id: 'country',
              name: 'País',
              values: [
                {
                  value: 'AU',
                  name: 'Australia',
                },
              ],
            },
            {
              id: 'timestamp',
              name: 'Data e Horário',
              validation: {
                primitiveType: 'STRING',
                entityType: 'DATE_TIME',
                pattern: 'yyyy-MM-ddTHH:mm:ss',
              },
            },
            {
              id: 'limit',
              name: 'Quantidade',
              validation: {
                primitiveType: 'INTEGER',
                min: 1,
                max: 50,
              },
            },
            {
              id: 'offset',
              name: 'Página',
              validation: {
                primitiveType: 'INTEGER',
              },
            },
          ],
      },
    })
    .mockRejectedValueOnce(new Error('Error')),
}));

jest.mock('next/router', () => ({
  useRouter: () => ({
    push: jest.fn(),
    query: {},
  }),
}));

describe('<PlaylistsFilters />', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should render "No filters available" when filter fields is empty', async () => {
    const { getByTestId, getByText } = render(
      <PlaylistsFilters />,
      { wrapper },
    );

    expect(getByTestId('loading-playlists-filters')).toBeInTheDocument();

    await waitFor(() => {
      expect(getByText(/No filters available/i)).toBeInTheDocument();
    });
  });

  it('should render filter fields', async () => {
    const {
      getByTestId,
      getByText,
    } = render(
      <PlaylistsFilters />,
      { wrapper },
    );

    expect(getByTestId('loading-playlists-filters')).toBeInTheDocument();

    await waitFor(() => {
      expect(getByTestId('locale')).toBeInTheDocument();
      expect(getByTestId('country')).toBeInTheDocument();
      expect(getByTestId('timestamp')).toBeInTheDocument();
      expect(getByTestId('limit')).toBeInTheDocument();
      expect(getByTestId('offset')).toBeInTheDocument();
      expect(getByText(/Get preferred playlists from iFood's customers/i)).toBeInTheDocument();
    });
  });

  it('should be able to call "console.log" with error', async () => {
    render(<PlaylistsFilters />, { wrapper });

    // Do Something when error ocoured on get Playlists Filters
    await waitFor(() => {
      // expect(spyConsole).toHaveBeenCalledTimes(1);
    });
  });
});
