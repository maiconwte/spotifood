import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

import Empty from '@/components/Empty';

describe('<Empty />', () => {
  it('should render with the default Empty title', () => {
    const { getByText } = render(<Empty />);

    expect(getByText(/Nothing to show/i)).toBeInTheDocument();
  });

  it('should render with a custom Empty title', () => {
    const { getByText } = render(<Empty title="Custom Title Here" />);

    expect(getByText(/Custom Title Here/i)).toBeInTheDocument();
  });

  it('should render with a custom Empty subtitle', () => {
    const { getByText } = render(<Empty subtitle="Custom Subtitle Here" />);

    expect(getByText(/Custom Subtitle Here/i)).toBeInTheDocument();
  });

  it('should render with a custom Empty title and subtitle', () => {
    const { getByText } = render(
      <Empty title="Custom Title with Subtitle" subtitle="Custom Subtitle with Title" />,
    );

    expect(getByText(/Custom Title with Subtitle/i)).toBeInTheDocument();
    expect(getByText(/Custom Subtitle with Title/i)).toBeInTheDocument();
  });
});
