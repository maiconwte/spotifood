import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';

import FeaturedPlaylists from '@/components/FeaturedPlaylists';

jest.mock('@/hooks/useFeaturedPlaylists', () => (
  jest.fn()
    .mockImplementationOnce(() => ({
      loading: true,
      title: 'should be able to render with loading status',
      playlists: [],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to render Empty state',
      playlists: [],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to render playlists items',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Playlist Name 1',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
        {
          id: 'playlist-id-2',
          name: 'Playlist Name 2',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 2',
        },
      ],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to filter local playlists by search input',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Playlist To Find',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
        {
          id: 'playlist-id-2',
          name: 'Playlist Name 2',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 2',
        },
      ],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to filter local playlists by search input',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Playlist To Find',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
        {
          id: 'playlist-id-2',
          name: 'Playlist Name 2',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 2',
        },
      ],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to render empty state when no one playlist has found by search input',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Any playlist',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
      ],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should be able to render empty state when no one playlist has found by search input',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Any playlist',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
      ],
      error: null,
    }))
    .mockImplementationOnce(() => ({
      loading: false,
      title: 'should render the error message',
      playlists: [
        {
          id: 'playlist-id-1',
          name: 'Any playlist',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
      ],
      error: new Error('Some error'),
    }))
));

describe('<FeaturedPlaylists />', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('should be able to render with loading status', () => {
    const { getByTestId } = render(
      <FeaturedPlaylists />,
    );

    expect(getByTestId(/loading-feature-playlists/i)).toBeInTheDocument();
  });

  it('should be able to render Empty state', () => {
    const { getByText } = render(
      <FeaturedPlaylists />,
    );

    expect(getByText(/Sorry. No playlist was found./i)).toBeInTheDocument();
    expect(getByText(/Try searching using other filters/i)).toBeInTheDocument();
  });

  it('should be able to render playlists items', () => {
    const { getAllByTestId } = render(
      <FeaturedPlaylists />,
    );

    const playlistNames = getAllByTestId('playlist-name')
      .map((i) => i.textContent);

    expect(playlistNames).toMatchInlineSnapshot(
      `
        Array [
          "Playlist Name 1",
          "Playlist Name 2",
        ]
    `,
    );
  });

  it('should be able to filter local playlists by search input', () => {
    const { getByPlaceholderText, getAllByTestId } = render(
      <FeaturedPlaylists />,
    );

    const searchInput = getByPlaceholderText('Search playlists by name');

    fireEvent.change(searchInput, { target: { value: 'Playlist To Find' } });

    const playlistNames = getAllByTestId('playlist-name')
      .map((i) => i.textContent);

    expect(playlistNames).toMatchInlineSnapshot(
      `
        Array [
          "Playlist To Find",
        ]
    `,
    );
  });

  it('should be able to render empty state when no one playlist has found by search input', () => {
    const { getByPlaceholderText, getByText } = render(
      <FeaturedPlaylists />,
    );

    const searchInput = getByPlaceholderText('Search playlists by name');
    const searchValue = 'No playlist';
    const emptyMessage = `No playlist found with name: "${searchValue}"`;

    fireEvent.change(searchInput, { target: { value: searchValue } });

    expect(getByText(emptyMessage)).toBeInTheDocument();
  });

  it('should render the error message', () => {
    const { getByText } = render(
      <FeaturedPlaylists />,
    );

    expect(getByText(/Something went wrong on fetch featured playlists items:/i)).toBeInTheDocument();
    expect(getByText(/Some error/i)).toBeInTheDocument();
  });
});
