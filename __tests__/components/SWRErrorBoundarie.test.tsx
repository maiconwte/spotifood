import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

import { SWRErrorBoundarie } from '@/components/Error';

describe('<SWRErrorBoundarie />', () => {
  it('should render with the Error message', () => {
    const message = 'custom Error message';
    const error = new Error(message);
    const { getByText, getByTestId } = render(<SWRErrorBoundarie error={error} />);

    expect(getByText(/message/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should render with custom title prop', () => {
    const title = 'custom title';
    const error = new Error('Error');
    const { getByText, getByTestId } = render(<SWRErrorBoundarie title={title} error={error} />);

    expect(getByText(/custom title/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should render with custom description prop', () => {
    const description = 'custom description';
    const error = new Error('Error');
    const { getByText, getByTestId } = render(
      <SWRErrorBoundarie description={description} error={error} />,
    );

    expect(getByText(/custom description/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should render with the custom title and description props', () => {
    const description = 'custom description';
    const title = 'custom title';
    const error = new Error('Error');
    const { getByText, getByTestId } = render(
      <SWRErrorBoundarie title={title} description={description} error={error} />,
    );

    expect(getByText(/custom title/i)).toBeInTheDocument();
    expect(getByText(/custom description/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should return the children component without no errors', () => {
    const { getByTestId } = render(
      <SWRErrorBoundarie error={null}>
        <div data-testid="children-component" />
      </SWRErrorBoundarie>,
    );

    expect(getByTestId('children-component')).toBeInTheDocument();
  });

  it('should render with a spotify token expired error', () => {
    const error = new Error('the access token expired');
    const { getByText, getByTestId } = render(
      <SWRErrorBoundarie error={error} />,
    );

    expect(getByText(/Login With Spotify/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should render with a spotify unsuported token error', () => {
    const error = new Error('only valid bearer authentication supported');
    const { getByText, getByTestId } = render(
      <SWRErrorBoundarie error={error} />,
    );

    expect(getByText(/Login With Spotify/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });

  it('should render with a spotify invalid token error', () => {
    const error = new Error('invalid access token');
    const { getByText, getByTestId } = render(
      <SWRErrorBoundarie error={error} />,
    );

    expect(getByText(/Login With Spotify/i)).toBeInTheDocument();
    expect(getByTestId('error-wrapper')).toBeInTheDocument();
  });
});
