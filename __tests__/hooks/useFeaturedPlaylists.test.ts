import { renderHook } from '@testing-library/react-hooks';
import formatISO from 'date-fns/formatISO';

import api from '@/services/api';
import useFeaturedPlaylists from '@/hooks/useFeaturedPlaylists';

jest.mock('@/services/api', () => ({
  get: jest.fn()
    .mockImplementationOnce(jest.fn())
    .mockImplementationOnce(jest.fn())
    .mockImplementationOnce(jest.fn())
    .mockResolvedValueOnce({
      data: {
        message: 'Playlists Title',
        playlists: {
          items: [
            {
              id: 'playlist-id-1',
              name: 'Playlist Name 1',
              images: [{ url: '' }],
              external_urls: { spotify: '' },
              description: 'Playlist Description 1',
            },
          ],
        },
      },
    })
    .mockRejectedValueOnce({
      response: {
        data: {
          error: {
            message: 'Some error occurred',
          },
        },
      },
    }),
}));

describe('useFeaturedPlaylists', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should call Spotify API with no query params', async () => {
    const query = {
      country: '',
      locale: '',
      timestamp: '',
      limit: 0,
      offset: 0,
    };

    const { waitForNextUpdate } = renderHook(() => useFeaturedPlaylists({ query }));

    const apiSpy = jest.spyOn(api, 'get');

    await waitForNextUpdate();

    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith('featured-playlists?');
  });

  it('should call Spotify API with query params', async () => {
    const query = {
      country: 'US',
      locale: 'sv_SE',
      timestamp: '',
      limit: 20,
      offset: 0,
    };

    const { waitForNextUpdate } = renderHook(() => useFeaturedPlaylists({ query }));

    const apiSpy = jest.spyOn(api, 'get');

    await waitForNextUpdate();

    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith('featured-playlists?country=US&locale=sv_SE&limit=20');
  });

  it('should be able to receive timestamp param on format "2020-08-06" and convert to ISO 8601 when query key is equal to "timestamp"', async () => {
    const query = {
      country: '',
      locale: '',
      timestamp: '2020-08-06',
      limit: 20,
      offset: 0,
    };

    const { waitForNextUpdate } = renderHook(() => useFeaturedPlaylists({ query }));

    const apiSpy = jest.spyOn(api, 'get');
    const parsedDate = encodeURIComponent(formatISO(new Date(query.timestamp)));

    await waitForNextUpdate();

    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith(`featured-playlists?timestamp=${parsedDate}&limit=20`);
  });

  it('should return fetched featured playlists items', async () => {
    const query = {
      country: '',
      locale: '',
      timestamp: '2020-08-06',
      limit: 20,
      offset: 0,
    };

    const { result, waitForNextUpdate } = renderHook(() => useFeaturedPlaylists({ query }));
    const apiSpy = jest.spyOn(api, 'get');

    await waitForNextUpdate();

    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(result.current.title).toStrictEqual('Playlists Title');
    expect(result.current.loading).toBe(false);
    expect(result.current.playlists).toStrictEqual(
      [
        {
          id: 'playlist-id-1',
          name: 'Playlist Name 1',
          images: [{ url: '' }],
          external_urls: { spotify: '' },
          description: 'Playlist Description 1',
        },
      ],
    );
  });

  it('should return state with error object', async () => {
    const query = {
      country: '',
      locale: '',
      timestamp: '',
      limit: 0,
      offset: 0,
    };

    const { result, waitForNextUpdate } = renderHook(() => useFeaturedPlaylists({ query }));

    const apiSpy = jest.spyOn(api, 'get');

    await waitForNextUpdate();

    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(result.current.loading).toEqual(false);
    expect(result.current.error.message).toStrictEqual('Some error occurred');
  });
});
