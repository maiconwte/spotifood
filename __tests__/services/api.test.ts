import axios from 'axios';
import { headers } from '@/services/api';

jest.mock('axios', () => ({
  create: jest.fn(),
}));

describe('api service', () => {
  it('should be an "axios" instance', () => {
    const spyAxios = jest.spyOn(axios, 'create');

    expect(spyAxios).toHaveBeenCalledWith({
      baseURL: 'https://api.spotify.com/v1/browse',
      headers,
    });
  });
});
