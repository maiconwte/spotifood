This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Spotifood Playlist App

[![tdd](https://img.shields.io/badge/maiconsilva-spotifood-blue)](https://maiconsilva.com) [![licence mit](https://img.shields.io/badge/Licence-MIT-green.svg)](https://github.com/maiconrs95/spotifood/blob/master/LICENSE.md) [![Build Status](https://travis-ci.com/maiconrs95/spotifood.svg?token=vvHxLDu1FJ3cgwN9VQca&branch=master)](https://travis-ci.com/maiconrs95/spotify-sdk-wrapper) [![Coverage Status](https://coveralls.io/repos/github/maiconrs95/spotifood/badge.svg?branch=master)](https://coveralls.io/github/maiconrs95/spotifood?branch=master)


## Acesso ao Spotify

Você pode acessar a home da aplicação e autenticar clicando no botão 'LOGIN WITH SPOTIFY'.

## Demo

Você pode conferir o projeto [nesse](https://spotifood.maiconrs95.vercel.app/) link.

## Rodando o projeto

Primeiro, instale as dependências:

```bash
npm i
# or
yarn
```

Agora rode o servidor de desenvolvimento:

```bash
npm run dev
# or
yarn dev
```

Abra [http://localhost:3000](http://localhost:3000) com o seu navegador para acessar a aplicação.

### Principais bibliotecas utilizadas no projeto

- [NextJS](https://nextjs.org/)
- [Typescript](https://nextjs.org/docs/basic-features/typescript)
- [SWR](https://github.com/vercel/swr)
- [Eslint](https://eslint.org/)
- [Husky](https://github.com/typicode/husky)
- [Jest](https://jestjs.io/docs/en/configuration)
- [@testing-library](https://testing-library.com/)
- [@testing-library/react-hooks](https://react-hooks-testing-library.com/)
- [styled-components](https://styled-components.com/)
- [Material UI](https://material-ui.com/)
- [Material Date Picker](https://material-ui-pickers.dev/)
- [date-fns](https://date-fns.org/)

### Estrtutura / Arquitetura

- `__tests__`:  Conteúdo de testes
- `src`: Conteúdo da aplicação
- `src/components`: Componentes reutilizavéis
- `src/componentes/Error`: Variações para componentes de erro
- `src/hooks`: Custom hooks reutilizáveis
- `src/pages`: Componentes com escopo de página.
- `src/services`: Facilitadores para integração com serviços externos, como comunicação com apis.
- `src/styles`: Componentes de estilo globais.
- `src/styles/pages`: Estilos customizados para páginas.
- `src/utils`: Funções utilitárias.

### SWR

A comunicação entre a api do spotify e o app é feita utilizando o [swr](https://github.com/vercel/swr).

Essa biblioteca permite re-validar os items da api em um intervalo de tempo. No caso do projeto a cada 30 segundos.

Também permite uma estratégia de cache, deixando a experiência do usuário mais "agradável".

O custom hook `useFeaturePlaylists` contém toda a abstração necessária para a view.

### Aplicação

- Os campos de "filtro de playlists" são montados com base [nesse](http://www.mocky.io/v2/5a25fade2e0000213aa90776) endpoint;
- A "lista de playlists" é montada com base [nessa](https://developer.spotify.com/documentation/web-api/reference/browse/get-list-featured-playlists/) referência da api do spotify;
- Sempre que um "filtro de api" é alterado, é feita uma nova chamada para a api do spotify;
- A "lista de playlists" é re-validada a cada 30 segundo pelo `SWR`.
- A "lista de playlists" contém um input para busca local por nome da playlist.

### Lighthouse

![Dashboard](.github/img/lighthouse-audits.jpeg)

### Testes

Para rodar os tests

```bash
npm run test
# or
yarn test
```

Para rodar os testes no modo `watch`:

```bash
npm run test:tdd
# or
yarn test:tdd
```

Para gerar o coverage report:

```bash
npm run test:coverage
# or
yarn test:coverage
```

### Build

```bash
npm run build
# or
yarn build
```

## Authors

* **Maicon Silva** - *Initial work* - [Site](https://maiconsilva.com)

## Authors

| ![Maicon Silva](https://avatars2.githubusercontent.com/u/19610095?v=3&s=150)|
|:---------------------:|
|  [Maicon Silva](https://github.com/spotifood/)   |

See also the list of [contributors](https://github.com/maiconrs95/spotifood/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
