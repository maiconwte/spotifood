module.exports = {
  moduleNameMapper: {
    '@/components/(.*)': '<rootDir>/src/components/$1',
    '@/hooks/(.*)': '<rootDir>/src/hooks/$1',
    '@/services/(.*)': '<rootDir>/src/services/$1',
  },
  setupFiles: ['jest-canvas-mock'],
  collectCoverageFrom: [
    '!**/node_modules/**',
    '!**/vendor/**',
    'src/components/**/*.{ts,tsx}',
    'src/hooks/**/*.{ts,tsx}',
    'src/services/**/*.ts',
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/src/components/SEO.tsx',
  ],
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 85,
      lines: 90,
      statements: -10,
    },
  },
};
